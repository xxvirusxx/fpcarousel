<?php
class ControllerExtensionModuleFpcarousel extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/fpcarousel');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_setting_module->addModule('fpcarousel', $this->request->post);
			} else {
				$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
			}
						
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		
		if (isset($this->error['thumb_width'])) {
			$data['error_thumb_width'] = $this->error['thumb_width'];
		} else {
			$data['error_thumb_width'] = '';
		}
		
		if (isset($this->error['thumb_height'])) {
			$data['error_thumb_height'] = $this->error['thumb_height'];
		} else {
			$data['error_thumb_height'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/fpcarousel', 'user_token=' . $this->session->data['user_token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/fpcarousel', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
			);			
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/fpcarousel', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/fpcarousel', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}
		
		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)&&isset($module_info['name'])) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		// name as title
		if (isset($this->request->post['name_as_title'])) {
			$data['name_as_title'] = $this->request->post['name_as_title'];
		} elseif (!empty($module_info)&&isset($module_info['name_as_title'])) {
			$data['name_as_title'] = $module_info['name_as_title'];
		} else {
			$data['name_as_title'] = 1;
		}
		
		// total products in carousel limit
		if (isset($this->request->post['limit'])) {
			$data['limit'] = $this->request->post['limit'];
		} elseif (!empty($module_info)&&isset($module_info['limit'])) {
			$data['limit'] = $module_info['limit'];
		} else {
			$data['limit'] = 8;
		}	

		// visible items per page
		if (isset($this->request->post['itemspage'])) {
			$data['itemspage'] = $this->request->post['itemspage'];
		} elseif (!empty($module_info)&&isset($module_info['itemspage'])) {
			$data['itemspage'] = $module_info['itemspage'];
		} else {
			$data['itemspage'] = 4;
		}
		
		// thumb width		
		if (isset($this->request->post['thumb_width'])) {
			$data['thumb_width'] = $this->request->post['thumb_width'];
		} elseif (!empty($module_info)&&isset($module_info['thumb_width'])) {
			$data['thumb_width'] = $module_info['thumb_width'];
		} else {
			$data['thumb_width'] = 180;
		}
		
		// thumb height	
		if (isset($this->request->post['thumb_height'])) {
			$data['thumb_height'] = $this->request->post['thumb_height'];
		} elseif (!empty($module_info)&&isset($module_info['thumb_height'])) {
			$data['thumb_height'] = $module_info['thumb_height'];
		} else {
			$data['thumb_height'] = 180;
		}		

		// shuffle products
		if (isset($this->request->post['shuffle_items'])) {
			$data['shuffle_items'] = $this->request->post['shuffle_items'];
		} elseif (!empty($module_info)&&isset($module_info['shuffle_items'])) {
			$data['shuffle_items'] = $module_info['shuffle_items'];
		} else {
			$data['shuffle_items'] = 1;
		}		

		// auto play
		if (isset($this->request->post['auto_play'])) {
			$data['auto_play'] = $this->request->post['auto_play'];
		} elseif (!empty($module_info)&&isset($module_info['auto_play'])) {
			$data['auto_play'] = $module_info['auto_play'];
		} else {
			$data['auto_play'] = 1;
		}	

		// pause on hover
		if (isset($this->request->post['pause_on_hover'])) {
			$data['pause_on_hover'] = $this->request->post['pause_on_hover'];
		} elseif (!empty($module_info)&&isset($module_info['pause_on_hover'])) {
			$data['pause_on_hover'] = $module_info['pause_on_hover'];
		} else {
			$data['pause_on_hover'] = 1;
		}	

		// show pagination
		if (isset($this->request->post['show_pagination'])) {
			$data['show_pagination'] = $this->request->post['show_pagination'];
		} elseif (!empty($module_info)&&isset($module_info['show_pagination'])) {
			$data['show_pagination'] = $module_info['show_pagination'];
		} else {
			$data['show_pagination'] = 1;
		}	

		// show pagination
		if (isset($this->request->post['show_navigation'])) {
			$data['show_navigation'] = $this->request->post['show_pagination'];
		} elseif (!empty($module_info)&&isset($module_info['show_navigation'])) {
			$data['show_navigation'] = $module_info['show_navigation'];
		} else {
			$data['show_navigation'] = 1;
		}
		
		// module status
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)&&isset($module_info['status'])) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		
		// carousel products		
		$this->load->model('catalog/product');
		
		$data['products'] = array();
		
		if (isset($this->request->post['product'])) {
			$products = $this->request->post['product'];
		} elseif (!empty($module_info)&&isset($module_info['product'])) {
			$products = $module_info['product'];
		} else {
			$products = array();
		}	
		
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}
				
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/fpcarousel', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/fpcarousel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if (!$this->request->post['thumb_width']) {
			$this->error['thumb_width'] = $this->language->get('error_thumb_width');
		}
		
		if (!$this->request->post['thumb_height']) {
			$this->error['thumb_height'] = $this->language->get('error_thumb_height');
		}
		
		return !$this->error;
	}
}
